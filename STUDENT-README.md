
## Rubric ##


### Abstract ### 

I will describe here my experience during this exercise,
to be taken as a whole.

The exercise consists in coding 6 callback functions and in tuning 
quite a lot of parameters. In my opinion, what is important in this exercise is to 
get the code nailed down 100%, but not to waste our precious time 
in tuning this to perfection. The tuning of a PID controller is its weakest point,
and we should not suffer it.

The 6 mentioned callbacks, which embody the controller for a model for a quad,
portray 5 methods for either P or PID controllers (C1-C5) and 1 method as a 
linear transformation in between total thrust and 3 commanded torques as input
and 4 individual thrusts as output (T1).

### Evolution of the derivation ###


T1. Linear transformation form collective thrust and torques into individual thrusts
- Not in the paper. Took about an hour to rewrite the equations from scratch 
(as I found the workbook unnecessary confusing on the notation) and to write the code.
- did not work at the beginning, struggled to find the reason why. Asked the mentor.
  Long time to get and answer from my mentor, which was that 'your signs are wrong'.
- what was wrong: you guys changed the motors order from the lecture to the skeleton code. 
  My mistake, I did not see it. But when asked, the mentor should had warned me, instead of 
  telling me that the signs were wrong. 
  I know, he was right, but I can think of a lot of right answers for that case 
  that would have been as useless.


The formulas for some of the controllers came from their derivation in the paper 
Feed Forward Parameter Identification for Precise Periodic Quadcopter Motions,
Angela Scoellig et al.

C1: Body rate

Not from the paper, only in the lecture and workbook. Straightforward.

C2: Yaw

Not from the paper, in the workbook. No issues.
Had to make sure to normalize the diff in angle into -pi,+pi. 

C3: Roll and pitch,

Very much mentioned in the paper.
Special mention to the fact that the formula for P controller 
r13c-dot = (r13-r13c)/tau 
is different between the paper and the workbook. 
The question about why there are two formulas with different sign
is still pending to be answered by the mentors. BTW, there should be a 
better connection in between mentors and the student blog. That's missing.

C4: Altitude

From the paper. No issues

C5: Lateral

From the paper. No issues





####Tuning

Spent hours tuning this system. This suggests to me an essential problem.

The instruction for submission advices about reading into some derivation 
for a cascade of two P controllers: the inner one nested inside the feedback 
loop of the outer one.
The instructions are to follow some final result in that derivation
for the ratio Kv/Kp in that context. 
Such advice does not apply to Kp,Kv for the case of a PD controller.
That was and is still confusing people. Check the p3 channel for signs 
of confusion.
I supplied to the list a block diagram for that derivation,
because without it is incomplete. 
I also offered the diagram to the author to be included in the derivation itself,
but never heard back.



####Test scenarios

There are 4 scenarios. Not clear if the last one is needed or it is optional.

Scenarios 2-4 all pass. The green box is popping up in all of them

Scenario 2 has an incorrect check on the angle. Why is it checked at all.
Angle is not controlled in that scenario, only omega. 

Scenario 5 is not passing, and I am unable to find a tuning strategy that would get me 
int track in any sensible amount of time.

Unless I get help from my mentor.
But the problem seems to be this: my mentor's mean response time is in the order of days.


### Response to first submission ###
I am supplying a zip file with the directory containing the .cpp code, the configs,
and this file. If unzipped over a working directory will certainly reproduce the claim above.

I was asked to tune the Ixx/Iyy/Izz. 
That does not make sense, here's why
(1) The values were given, nowhere it says we should change them.
(2) the values are absolutely compatible with both the total mass and the sizes. Positive.

I was asked to further tune kPozZ, kVelZ, I did it.

It was pointed out that the majority of the scenarios fail. 
Only Scenario 5 is failing. The drone is going along the figure 8, closer in the FF case,
but apparently not close enough.






### My follow up questions for the reviewer ###

(1) Is the code correct?. 

Note that I am asking about the code, not the tuning. 
If the code is not correct, here is my second question:

(2) what exactly is missing or otherwise wrong?
