

## Rubric ##


### Abstract ### 

This is an addendum to the first write up, still inside the companion file STUDENT_README.md.

For this follow up, and as requested, the write up has been moved close to where the code is written.

Please find the write up in each method inside class QuadControl.cpp


### Response to previous review ###
- in the code: removed integrator in XY dimension
- in the code: added extensive write up
- in the config file: incorporated suggested changes for the tuning parameters
- observed that all scenarios pass


### My follow up questions for the reviewer ###

No questions at the moment.

